const fs = require('fs').promises;

exports.handleError = function handleError(err, cmd, args, msg, client, config) {
  return new Promise(async function(resolve) {
    console.error(err);    
    try {
      msg.reply(`An error has occured:\n\`\`\`\n${err}\n\`\`\``);
    } catch(err) {}
    if(config.dmForErrors && config.owner) {
      try {
        let user = await client.users.fetch(config.owner);
        await user.send(`An error has occured:\nCommand: ${cmd}\nArguements: ${args.join(' ')}\nLink: ${msg.url}\n\`\`\`\n${err}\n\`\`\``);
      } catch(err) {}
    }
    resolve();
  });
}

exports.updateConfig = function updateConfig(config) {
  return new Promise(async function(resolve) {
    await fs.writeFile('./config.json', JSON.stringify(config));
    resolve();
  });
}

exports.updateMedia = function updateMedia(media) {
  return new Promise(async function(resolve) {
    await fs.writeFile('./bingus-media.json', JSON.stringify(media));
    resolve();
  });
}
