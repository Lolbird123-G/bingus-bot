const discord = require('discord.js');
const utils = require('../utils.js');
const media = require('../bingus-media.json').media;

exports.info = {
  name: 'bingus',
  hidden: false,
  usage: 'bingus',
  desc: 'Sends a random bingus gif/image/video.'
};

exports.exec = function exec(args, msg, client, config) {
  return new Promise(async function(resolve) {
    let m = media[Math.floor(Math.random() * media.length)];
    try {
      await msg.reply(m);
    } catch(err) {}
    resolve();
  });
}
