const discord = require('discord.js');
const utils = require('../utils.js');

exports.info = {
  name: 'eval',
  hidden: true,
  usage: 'eval <code>',
  desc: ''
};

exports.exec = function exec(args, msg, client, config) {
  return new Promise(async function(resolve) {
    if(msg.author.id === config.owner) {
      let code = args.join(' ');
      let evaled;
      try {
        evaled = eval(code);
      } catch(err) {
        try {
          await msg.reply(`Error:\n\`\`\`\n${err}\n\`\`\``);
        } catch(err) {}
        resolve();
        return;
      }
      
      if(typeof evaled === 'object' && evaled instanceof Promise) {
        try {
          evaled = await evaled;
        } catch(err) {
          try {
            await msg.reply(`Error:\n\`\`\`\n${err}\n\`\`\``);
          } catch(err) {}
          resolve();
          return;
        }
        
        if(typeof evaled === 'object') {
          try {
            evaled = JSON.stringify(evaled);
          } catch(err) {}
          try {
            await msg.reply(`Output (promise result):\n\`\`\`\n${evaled}\n\`\`\``);
          } catch(err) {}
        } else {
          try {
            await msg.reply(`Output (promise result):\n\`\`\`\n${evaled}\n\`\`\``);
          } catch(err) {}
        }
      } else if(typeof evaled === 'object') {
        try {
          evaled = JSON.stringify(evaled);
        } catch(err) {}
        try {
          await msg.reply(`Output:\n\`\`\`\n${evaled}\n\`\`\``);
        } catch(err) {}
      } else {
        await msg.reply(`Output:\n\`\`\`\n${evaled}\n\`\`\``);
      }
    } else {
      try {
        await msg.reply('Access denied.');
      } catch(err) {}
    }
    resolve();
  });
}
