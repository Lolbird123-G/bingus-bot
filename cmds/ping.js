const discord = require('discord.js');
const utils = require('../utils.js');
const media = require('../bingus-media.json').media;

exports.info = {
  name: 'ping',
  hidden: false,
  usage: 'ping',
  desc: 'Used to test time for communication between bot and discord, or checking if the bot is online.'
};

exports.exec = function exec(args, msg, client, config) {
  return new Promise(async function(resolve) {
    try {
      await msg.reply(`Pong! ${client.ws.ping}ms`);
    } catch(err) {}
    resolve();
  });
}
