const discord = require('discord.js');
const utils = require('../utils.js');

exports.info = {
  name: 'le_troll',
  hidden: true,
  usage: 'le_troll <message>',
  desc: ''
};

exports.exec = function exec(args, msg, client, config) {
  return new Promise(async function(resolve) {
    if(config.le_troll.enabled && config.le_troll.channel) {
      let access = false;
      for(let i=0; i<config.le_troll.roles.length; i++) if(msg.member.roles.cache.has(config.le_troll.roles[i])) access = true;
      
      if(msg.guild.id === config.server && access) {
        if(args.join(' ')) {
          if(config.le_troll.log) console.log(`User ${msg.author.tag} (${msg.author.id}) used le_troll for: ${args.join(' ')}`);
          try {
            let channel = await client.channels.fetch(config.le_troll.channel);
            await channel.send(args.join(' '), {
              allowedMentions: {
                parse: ['users'],
                roles: [],
                repliedUser: false
              }
            });
          } catch(err) {}
        } else {
          try {
            await msg.reply('You must specify a message.');
          } catch(err) {}
        }
      } else {
        try {
          await msg.reply('Access denied');
        } catch(err) {}
      }
    } else {
      try {
        await msg.reply('Command disabled');
      } catch(err) {}
    }
    resolve();
  });
}
