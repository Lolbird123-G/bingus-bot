const fs = require('fs').promises;
const discord = require('discord.js');
const utils = require('../utils.js');

exports.info = {
  name: 'help',
  hidden: false,
  usage: 'help [command]',
  desc: 'Provides information on a given command, or a list of commands, if none given.'
};

exports.exec = function exec(args, msg, client, config) {
  return new Promise(async function(resolve) {
    if(args[0]) {
      let cmd = args[0].replace(/[^a-z0-9-_]/gi, '');
      let c;
      try {
        c = require(`./${cmd}.js`).info;
      } catch(err) {}
      
      if(c && !c.hidden) {
        let info = '**Command info:**\n';
        if(c.name) info += `**Name:** ${c.name}\n`;
        if(c.usage) info += `**Usage:** ${config.prefix}${c.usage}\n`;
        if(c.desc) info += `**Description:** ${c.desc}\n`;
        try {
          await msg.reply(info);
        } catch(err) {}
      } else {
        await msg.reply('Unknown command.');
      }
    } else {
      let cmds = await fs.readdir('./cmds/');
      let list = '__**Commands:**__\n```\n';
      for(let i=0; i<cmds.length; i++) {
        try {
          let name = cmds[i].slice(0, -3);
          let c = require(`./${name}.js`).info;
          if(c.name && c.usage && c.desc && !c.hidden) {
            list += `${config.prefix}${c.usage} : ${c.desc}\n`;
          }
        } catch(err) {}
      }
      list += '```';
      
      let out = 'Bingus my beloved <3\n';
      if(config.links.bot) out += `**Invite me:** <${config.links.bot}>\n`;
      if(config.links.server) out += `**Support server:** <${config.links.server}>\n`;
      if(config.links.repo) out += `**Source code:** <${config.links.repo}>\n`;
      out += list;
      
      try {
        await msg.reply(out);
      } catch(err) {}
    }
    resolve();
  });
}
