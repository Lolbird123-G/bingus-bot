const discord = require('discord.js');
const utils = require('../utils.js');
const media = require('../bingus-media.json').special;

exports.info = {
  name: 'dance',
  hidden: false,
  usage: 'dance',
  desc: 'Sends binglet dancing video.'
};

exports.exec = function exec(args, msg, client, config) {
  return new Promise(async function(resolve) {
    try {
      await msg.reply(media.dance);
    } catch(err) {}
    resolve();
  });
}
