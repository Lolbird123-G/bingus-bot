const discord = require('discord.js');
const utils = require('../utils.js');
const media = require('../bingus-media.json');

exports.info = {
  name: 'bingus-media',
  hidden: true,
  usage: '',
  desc: ''
};

exports.exec = function exec(args, msg, client, config) {
  return new Promise(async function(resolve) {
    if(msg.author.id === config.owner) {
      let action = args.shift();
      
      if(action === 'list') {
        let out = 'Media:\n```\n';
        for(let i=0; i<media.media.length; i++) out += `${media.media[i]}\n`;
        out += '```';
        try {
          await msg.reply(out);
        } catch(err) {}
      } else if(action === 'add') {
        if(args[0]) {
          media.media.push(args[0]);
          await utils.updateMedia(media);
          try {
            await msg.reply('Done.');
          } catch(err) {}
        } else {
          try {
            await msg.reply('No link specified.')
          } catch(err) {}
        }
      } else if(action === 'remove') {
        if(args[0]) {
          media.media = media.media.filter(function(i) {
            return i !== args[0]
          });
          await utils.updateMedia(media);
          try {
            await msg.reply('Done.');
          } catch(err) {}
        } else {
          try {
            await msg.reply('No link specified.')
          } catch(err) {}
        }
      } else {
        msg.reply('Unknown action.');
      }
    } else {
      try {
        await msg.reply('Access denied.');
      } catch(err) {}
    }
    resolve();
  });
}
