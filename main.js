const discord = require('discord.js');
const utils = require('./utils.js');
const config = require('./config.json');
const client = new discord.Client({
  allowedMentions: { repliedUser: false },
  presence: {
    activities: [{
      name: config.presence.replaceAll('[prefix]', config.prefix),
      type: 0
    }]
  },
  intents: [512, 1, 32768]
});

client.on('ready', function() {
  console.log(`Ready as ${client.user.tag} (${client.user.id})`);
});

client.on('messageCreate', async function(msg) {
  if(msg.author.bot) return;
  if(msg.content.startsWith(config.prefix)) {
    let args = msg.content.slice(config.prefix.length).split(' ');
    let cmd = args.shift().replace(/[^a-z0-9-_]/gi, '');
    let c;
    
    try {
      c = require(`./cmds/${cmd}.js`);
    } catch(err) {}
    
    if(c) {
      try {
        await c.exec(args, msg, client, config);
      } catch(err) {
        await utils.handleError(err, cmd, args, msg, client, config);
      }
    }
  } else if((config.bingusReact.enabled && config.bingusReact.emoteString) &&
            (msg.content.toLowerCase() === 'bingus' || 
            (msg.content.toLowerCase().includes('bingus') && config.bingusReact.catchesSubstrings))) {
    try {
      await msg.react(config.bingusReact.emoteString);
    } catch(err) {}
  }
});

client.on('messageDelete', async function(msg) {
  if(msg.author.bot) return;
  if(config.attachmentLogging.enabled && config.attachmentLogging.channel && config.server) {
    if(msg.attachments.size > 0 && msg.guild.id === config.server) {
      try {
        let channel = await client.channels.fetch(config.attachmentLogging.channel);
        let files = [];
        msg.attachments.each(file => {files.push(file.url)});
        
        await channel.send({
          content: `Deleted message (${msg.id}) from ${msg.author.tag} (${msg.author.id}) attachments:`,
          files: files
        });
      } catch(err) {}
    }
  }
});

client.login(config.token);
