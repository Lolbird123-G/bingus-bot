while [ true ]; do
  echo -e "\033[1;31mStarting bot...\033[0m"
  node .
  echo -e "\033[1;Bot crashed or process exited, restarting in 3 seconds...\033[0m"
  sleep 3
done
